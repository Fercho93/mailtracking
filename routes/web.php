<?php

use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    Mail::to('pruebanueva@prueba.com')->send(new TestMail());

    return view('welcome');
});


Route::post('sns', 'MailTracking@sns_notifications');
Route::get('show/{id}', 'Api\EmailController@show');
Route::post('checkMail', 'Api\EmailController@checkMail');
