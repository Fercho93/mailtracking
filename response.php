<?php

$response = [
    'Type' => 'Notification',
    'MessageId' => 'f7144300-ee20-5c82-a30f-1971bfd21926',
    'TopicArn' => 'arn:aws:sns:us-east-1:542969640450:cpanel',
    'Subject' => 'Amazon SES Email Event Notification',
    'Message' => '
        {
            "eventType":"Send",
            "mail":
                {
                    "timestamp":"2022-01-31T02:55:46.304Z",
                    "source":"sandovalf193@gmail.com",
                    "sourceArn":"arn:aws:ses:us-east-1:542969640450:identity/sandovalf193@gmail.com",
                    "sendingAccountId":"542969640450",
                    "messageId":"0100017eae0f5080-d64a85ea-99af-4305-acbc-ea3c888dccd9-000000",
                    "destination":["sandovalf193@gmail.com"],
                    "headersTruncated":false,
                    "headers":
                        [
                            {
                                "name":"Message-ID",
                                "value":"<ac942ed2aee8a4dd5b8638dd2ab09fba@emailtraking.sandovalf.com>"
                            },
                            {
                                "name":"Date",
                                "value":"Mon, 31 Jan 2022 02:55:45 +0000"
                            },
                            {
                                "name":"Subject",
                                "value":"Test Mail"
                            },
                            {
                                "name":"From",
                                "value":"Laravel <sandovalf193@gmail.com>"
                            },
                            {
                                "name":"To",
                                "value":"sandovalf193@gmail.com"
                            },
                            {
                                "name":"MIME-Version",
                                "value":"1.0"
                            },
                            {
                                "name":"Content-Type",
                                "value":"text/html; charset=utf-8"
                            },
                            {
                                "name":"Content-Transfer-Encoding",
                                "value":"quoted-printable"
                            },
                            {
                                "name":"unique-id",
                                "value":"asdfasdfy98ashs9u"
                            }
                        ],
                        "commonHeaders":
                            {
                                "from": ["Laravel <sandovalf193@gmail.com>"],
                                "date":"Mon, 31 Jan 2022 02:55:45 +0000",
                                "to":["sandovalf193@gmail.com"],
                                "messageId":"0100017eae0f5080-d64a85ea-99af-4305-acbc-ea3c888dccd9-000000",
                                "subject":"Test Mail"
                            },
                        "tags":
                            {
                                "ses:operation":["SendRawEmail"],
                                "ses:configuration-set":["prueba-mail"],
                                "ses:source-ip":["162.252.57.68"],
                                "ses:from-domain":["gmail.com"],
                                "ses:caller-identity":["ses-ejemplo"]
                            }
                },
            "send":{}}
  ',
    'Timestamp' => '2022-01-31T02:55:46.646Z',
    'SignatureVersion' => '1',
    'Signature' => 'jLRJj026l0Kvwv0Zb1fW2vFwpW3qnW7SAPgj9zrQXAyqlCvHeYKapw8gS1xXxzIJHRucvC2YJJMIr0CiWM4sHCNAt/efsrFqZ/WUipdmAxwdBkoM4C3lnTreA0cUmz1W/Faftj0iEusfQ4YbrDELNLfDNzOEaWpUynIwFrQOfx6GrmBcbSqkdPxpyvrnXB/CTZTbJtIfh5qnW9cBrfHzAmT9KlhY9IBQ3i26qkxkpTs58u5ZPhdqFIZAWpVdPTTeErBGxnBIAO8/74iXJhks5AgVboVssb6C+Yh0zbbKn+XpIBV9lPWwK1xLctQ+Ql5DfryCWAMXoKTnSqePyfHQMQ==',
    'SigningCertURL' => 'https://sns.us-east-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem',
    'UnsubscribeURL' => 'https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:542969640450:cpanel:1a1afe23-6e3f-44ed-8771-2874049dc1bc',
];


$response2 = array (
    'eventType' => 'Delivery',
    'mail' =>
    array (
      'timestamp' => '2022-02-10T02:36:31.087Z',
      'source' => 'notificaciones_imassclick@imass.com.mx',
      'sourceArn' => 'arn:aws:ses:us-west-2:197773543549:identity/notificaciones_imassclick@imass.com.mx',
      'sendingAccountId' => '197773543549',
      'messageId' => '0101017ee17d47ef-127b2007-0524-4334-975d-afa0df3c52e5-000000',
      'destination' =>
      array (
        0 => 'sandovalf193@gmail.com',
      ),
      'headersTruncated' => false,
      'headers' =>
      array (
        0 =>
        array (
          'name' => 'Message-ID',
          'value' => '<f638bbf73a31680bdcca1d5bb5d87c60@emailtraking.sandovalf.com>',
        ),
        1 =>
        array (
          'name' => 'Date',
          'value' => 'Thu, 10 Feb 2022 02:36:30 +0000',
        ),
        2 =>
        array (
          'name' => 'Subject',
          'value' => 'prueba de correo',
        ),
        3 =>
        array (
          'name' => 'From',
          'value' => 'Laravel <notificaciones_imassclick@imass.com.mx>',
        ),
        4 =>
        array (
          'name' => 'To',
          'value' => 'sandovalf193@gmail.com',
        ),
        5 =>
        array (
          'name' => 'MIME-Version',
          'value' => '1.0',
        ),
        6 =>
        array (
          'name' => 'Content-Type',
          'value' => 'text/html; charset=utf-8',
        ),
        7 =>
        array (
          'name' => 'Content-Transfer-Encoding',
          'value' => 'quoted-printable',
        ),
        8 =>
        array (
          'name' => 'unique-id',
          'value' => '50e22240-1000-407d-9527-4b9f90efcfce',
        ),
      ),
      'commonHeaders' =>
      array (
        'from' =>
        array (
          0 => 'Laravel <notificaciones_imassclick@imass.com.mx>',
        ),
        'date' => 'Thu, 10 Feb 2022 02:36:30 +0000',
        'to' =>
        array (
          0 => 'sandovalf193@gmail.com',
        ),
        'messageId' => '0101017ee17d47ef-127b2007-0524-4334-975d-afa0df3c52e5-000000',
        'subject' => 'prueba de correo',
      ),
      'tags' =>
      array (
        'ses:operation' =>
        array (
          0 => 'SendRawEmail',
        ),
        'ses:configuration-set' =>
        array (
          0 => 'ImassClick',
        ),
        'ses:source-ip' =>
        array (
          0 => '162.252.57.68',
        ),
        'ses:from-domain' =>
        array (
          0 => 'imass.com.mx',
        ),
        'ses:caller-identity' =>
        array (
          0 => 'aldair',
        ),
        'ses:outgoing-ip' =>
        array (
          0 => '54.240.27.22',
        ),
      ),
    ),
    'delivery' =>
    array (
      'timestamp' => '2022-02-10T02:36:31.955Z',
      'processingTimeMillis' => 868,
      'recipients' =>
      array (
        0 => 'sandovalf193@gmail.com',
      ),
      'smtpResponse' => '250 2.0.0 OK  1644460591 d12si18969523pfv.38 - gsmtp',
      'reportingMTA' => 'a27-22.smtp-out.us-west-2.amazonses.com',
    ),
);
