<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Str;
use Illuminate\Queue\SerializesModels;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.test')->withSwiftMessage(function ($message) {
            $message->getHeaders()->addTextHeader('x-ses-configuration-set', config('services.ses.configuration_set'));
            $message->getHeaders()->addTextHeader('unique-id', Str::uuid());
        })->subject('prueba de correo');
    }
}
