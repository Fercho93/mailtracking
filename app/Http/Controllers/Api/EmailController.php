<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\EmailBlackList;
use App\Models\EmailTracking;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function checkMail(Request $request)
    {
        $email = EmailBlackList::where('email_address',$request->email)->first();

        return response()->json([
            'message' =>  $email ? 'Email is blacklisted' : 'Email is not blacklisted',
            'status' => $email ? true : false,
        ]);
    }

    public function show(Request $request, $id)
    {
        $data = EmailTracking::where('message_id', $id)->get();

        $res = [
            'events' => [],
        ];

        foreach ($data as $key => $value) {
            $res['to']          = $value->to;
            $res['messageId']   = $value->message_id;
            $res['subject']     = $value->subject;
            $res['events'][]    = [
                'date'          => $value->date,
                'event_type'    => $value->event_type,
            ];
        }

        return response()->json($res);
    }
}
