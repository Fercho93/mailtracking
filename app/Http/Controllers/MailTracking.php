<?php

namespace App\Http\Controllers;

use App\Models\EmailBlackList;
use App\Models\EmailTracking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MailTracking extends Controller
{
    public function sns_notifications(Request $request)
    {
        $data = $request->json()->all();

        if (isset($data['SubscribeURL'])){
            file_get_contents($data['SubscribeURL']);  // To verify. (When the first request comes from AWS SNS)
        }

        Log::info($data);
        if(isset($data['eventType'])) {

            if($data['eventType'] === 'Bounce') {
                $this->addMailToBlackList($data);
            }

            $delayType = isset($data['deliveryDelay']['delayType']) ? $data['deliveryDelay']['delayType'] : null;
            if($data['eventType'] === 'DeliveryDelay' && $delayType === 'TransientCommunicationFailure') {
                $this->addMailToBlackList($data);
            }


            $notificationData = [];
            foreach ($data['mail']['headers'] as $item) {
                if($item['name'] === 'To') $notificationData['to'] = $item['value'];
                if($item['name'] === 'Subject') $notificationData['subject'] = $item['value'];
                if($item['name'] === 'unique-id') $notificationData['message_id'] = $item['value'];
            }
            $timestamp = count($data[$this->stringToKebakCase($data['eventType'])]) > 0 ? $data[$this->stringToKebakCase($data['eventType'])]['timestamp'] : $data['mail']['timestamp'];

            EmailTracking::create([
                'to'            => $notificationData['to'],
                'date'          => $timestamp,
                'subject'       => $notificationData['subject'],
                'message_id'    => $notificationData['message_id'],
                'event_type'    => $data['eventType'],
                'delay_type'    => $delayType
            ]);
        }

        return response('OK', 200);
    }

    public function stringToKebakCase($string)
    {
        $firstLetter = strtolower($string[0]);
        $restLetters = substr($string, 1);
        return $firstLetter . $restLetters;
    }

    public function addMailToBlackList($data)
    {
        $email_address = '';
        foreach ($data['mail']['headers'] as $item) {
            if($item['name'] === 'To') {
                $email_address = $item['value'];
            }
        }

        $email = EmailBlackList::where('email_address', $email_address)->first();

        if(!$email) {
            EmailBlackList::create([
                'email_address' => $email_address,
            ]);
        }
    }
}
