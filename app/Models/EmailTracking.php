<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailTracking extends Model
{
    use HasFactory;

    protected $fillable = [
        'to',
        'date',
        'subject',
        'message_id',
        'event_type',
        'delay_type',
    ];
}
